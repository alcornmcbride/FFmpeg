/*
 * AMT image encoder
 * Copyright (c) 2017-2018 Alcorn McBride Inc.
 *
 */
 
#include "libavutil/imgutils.h"
#include "libavutil/internal.h"
#include "libavutil/intreadwrite.h"
#include "libavutil/opt.h"
#include "libavutil/pixdesc.h"
#include "avcodec.h"
#include "internal.h"
#include "amt.h"

typedef struct AMTContext {
	AVClass *class;

	/* basic video info which is common to all frames */
	int width, height;
	int pixel_format;
	int chroma_type;
	int padding_type;
	int colorspace, color_range;

	/* encode method for an individual frame */
	int (*encode)(struct AMTContext*, uint8_t* out, const AVFrame *pic);
} AMTContext;

/*****************************************************************************/
static int amt_encode_bgr8(AMTContext *this, uint8_t *outbuf, const AVFrame *pic)
{
	const int pixelsize = 3;
	int i, n = pixelsize * this->width;
	uint8_t *out = outbuf;
	const uint8_t *ptr = pic->data[0];

	for (i = 0; i < this->height; i++) {
		const uint8_t *src = ptr;

		/* always use RidePlayer native padding when encoding */
		const int wordsize = 16;

		/* pack up to five 24-bit pixels into 128-bit words (RidePlayer native padding) */
		while (src < ptr + n) {
			/* how many more bytes are in the current word? */
			int bytes, maxbytes = wordsize - ((out - outbuf) % wordsize);
			
			/* copy as many remaining pixels as we can fit into the current word */
			bytes = FFMIN((maxbytes / pixelsize) * pixelsize, ptr + n - src);
			memcpy(out, src, bytes);
			src += bytes;
			out += bytes;

			/* no more room in word -> skip to next one */
			if (maxbytes - bytes < pixelsize) {
				out += (maxbytes - bytes);
			}
		}
		
		ptr += pic->linesize[0];
	}

	return out - outbuf;
}

/*****************************************************************************/
static int amt_encode_yuv10(AMTContext *this, uint8_t *outbuf, const AVFrame *pic)
{
	int line = 0, pixel = 0, i;
	uint32_t val;

	uint32_t *out = (uint32_t *)outbuf;
	const uint16_t *y = (const uint16_t *)pic->data[0];
	const uint16_t *u = (const uint16_t *)pic->data[1];
	const uint16_t *v = (const uint16_t *)pic->data[2];

	while (line < this->height)
	{
		/* TODO: handle other chroma types */
		uint16_t x_y[6];
		uint16_t x_cb[3];
		uint16_t x_cr[3];
		/* pack 6 pixels into a 128-bit word */
		for (i = 0; i < 6; i++)
		{
			x_y[i] = y[pixel];
			x_cb[i >> 1] = u[pixel >> 1];
			x_cr[i >> 1] = v[pixel >> 1];

			/* end of scanline: continue filling word from beginning of next line */
			if (++pixel == this->width) {
				y += pic->linesize[0] >> 1;
				u += pic->linesize[1] >> 1;
				v += pic->linesize[2] >> 1;

				pixel = 0;
				if (++line == this->height)
				{
					/* end of frame: leave rest of word alone */
					break;
				}
			}
		}

		val = (x_cb[0] << 0) | (x_y[0] << 10) | (x_cr[0] << 20) | (x_y[1] << 30);
		AV_WL32(out, val);
		out++;
		val = (x_y[1] >> 2) | (x_cb[1] << 8) | (x_y[2] << 18) | (x_cr[1] << 28);
		AV_WL32(out, val);
		out++;
		val = (x_cr[1] >> 4) | (x_y[3] << 6) | (x_cb[2] << 16) | (x_y[4] << 26);
		AV_WL32(out, val);
		out++;
		val = (x_y[4] >> 6) | (x_cr[2] << 4) | (x_y[5] << 14);
		AV_WL32(out, val);
		out++;
	}

	return (uint8_t *)out - outbuf;
}

/*****************************************************************************/
static int amt_encode_frame(AVCodecContext *avctx, AVPacket *pkt,
							const AVFrame *p, int *got_packet)
{
	AMTContext *s = avctx->priv_data;
	int picsize, datasize = -1, ret;
	uint8_t *out;
	AMTHeader_t *header;
	
	picsize = av_image_get_buffer_size(avctx->pix_fmt, avctx->width, avctx->height, 1);

	/* account for 128-bit padding */
	if (avctx->pix_fmt == AV_PIX_FMT_BGR24)
	{
		picsize = ((picsize + 15) / 15) * 16;
	}
		
	avctx->bits_per_coded_sample = av_get_bits_per_pixel(av_pix_fmt_desc_get(avctx->pix_fmt));

	if ((ret = ff_alloc_packet2(avctx, pkt, picsize + 512, 0)) < 0)
		return ret;
	
	memset(pkt->data, 0, pkt->size);
	
	header = (AMTHeader_t *)pkt->data;
	out    = pkt->data + 512;
	
	if ((datasize = s->encode(s, out, p)) < 0)
		return datasize;

	pkt->size = datasize + 512;
	pkt->flags |= AV_PKT_FLAG_KEY;
	*got_packet = 1;

	/* set up AMT header information */
	memcpy(header->id, AMT_FORMAT_ID, 4);
	AV_WL16(&header->version_major, AMT_FORMAT_VERSION_MAJOR);
	AV_WL16(&header->version_minor, AMT_FORMAT_VERSION_MINOR);
	AV_WL32(&header->file_size, pkt->size);
	AV_WL32(&header->width, avctx->width);
	AV_WL32(&header->height, avctx->height);
	AV_WL32(&header->pixel_format, s->pixel_format);
	AV_WL32(&header->chroma_type, s->chroma_type);
	AV_WL32(&header->padding_type, s->padding_type);
	AV_WL32(&header->image_size, datasize);
	AV_WL32(&header->color_range, s->color_range);
	AV_WL32(&header->colorspace, s->colorspace);
	
	return 0;
}

/*****************************************************************************/
static av_cold int amt_encode_init(AVCodecContext *avctx)
{
	AMTContext *s = avctx->priv_data;
	int use_colorspace = 0;

	if (avctx->width > 0xffff || avctx->height > 0xffff) {
		av_log(avctx, AV_LOG_ERROR, "image dimensions too large\n");
		return AVERROR_INVALIDDATA;
	}

	s->width = avctx->width;
	s->height = avctx->height;
	s->padding_type = AMT_PADDING_NATIVE;
	s->chroma_type = AMT_CHROMA_444;
	s->colorspace = AMT_COLOR_REC_709;
	s->color_range = AMT_COLOR_RANGE_FULL;
		
	/* Pixel format */
	switch (avctx->pix_fmt) {
	case AV_PIX_FMT_BGR24:
		s->pixel_format = AMT_FMT_BGR8;
		s->encode = amt_encode_bgr8;
		break;

	case AV_PIX_FMT_YUV422P10:
		s->pixel_format = AMT_FMT_YUV10;
		s->chroma_type = AMT_CHROMA_422;
		s->color_range = AMT_COLOR_RANGE_PARTIAL;
		s->encode = amt_encode_yuv10;
		use_colorspace = 1;
		break;

	default:
		av_log(avctx, AV_LOG_ERROR, "Pixel format '%s' not supported.\n",
			av_get_pix_fmt_name(avctx->pix_fmt));
		return AVERROR_INVALIDDATA;
	}
	
	/* YUV colorspace settings */
	if (use_colorspace) {
		switch (avctx->colorspace) {
		/* TODO: DCI-P3 */
		case AVCOL_SPC_UNSPECIFIED:
		case AVCOL_SPC_BT709:
			s->colorspace = AMT_COLOR_REC_709;
			break;
			
		case AVCOL_SPC_SMPTE170M:
		case AVCOL_SPC_BT470BG:
			s->colorspace = AMT_COLOR_REC_601;
			break;
		
		case AVCOL_SPC_BT2020_NCL:
			s->colorspace = AMT_COLOR_REC_2020;
			break;
			
		default:
			av_log(avctx, AV_LOG_ERROR, "Color space '%s' not supported.\n",
				av_color_space_name(avctx->colorspace));
			return AVERROR_INVALIDDATA;
		}
		
		if (avctx->color_range == AVCOL_RANGE_JPEG)
		{
			s->color_range = AMT_COLOR_RANGE_FULL;
		}
	}
	
	/* TODO: scan type */
	
#if FF_API_CODED_FRAME
FF_DISABLE_DEPRECATION_WARNINGS
	avctx->coded_frame->key_frame = 1;
	avctx->coded_frame->pict_type = AV_PICTURE_TYPE_I;
FF_ENABLE_DEPRECATION_WARNINGS
#endif

	return 0;
}

/*****************************************************************************/
#define OFFSET(x) offsetof(AMTContext, x)
#define VE AV_OPT_FLAG_VIDEO_PARAM | AV_OPT_FLAG_ENCODING_PARAM
static const AVOption amt_options[] = {
	/* TODO: do we want any encoding options? see targaenc.c, etc. for examples */
	{ NULL },
};

static const AVClass amt_class = {
	.class_name = "amt",
	.item_name  = av_default_item_name,
	.option     = amt_options,
	.version    = LIBAVUTIL_VERSION_INT,
};

AVCodec ff_amt_encoder = {
	.name           = "amt",
	.long_name      = NULL_IF_CONFIG_SMALL("Alcorn McBride AMT image"),
	.type           = AVMEDIA_TYPE_VIDEO,
	.id             = AV_CODEC_ID_AMT,
	.priv_data_size = sizeof(AMTContext),
	.priv_class     = &amt_class,
	.init           = amt_encode_init,
	.encode2        = amt_encode_frame,
	.pix_fmts       = (const enum AVPixelFormat[]){
		AV_PIX_FMT_BGR24, 
		AV_PIX_FMT_YUV422P10,
		/* TODO: others? */
		AV_PIX_FMT_NONE
	},
};
