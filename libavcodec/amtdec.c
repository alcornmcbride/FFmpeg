/*
* AMT image decoder
* Copyright (c) 2017-2018 Alcorn McBride Inc.
*
*/

#include "libavutil/imgutils.h"
#include "libavutil/internal.h"
#include "libavutil/intreadwrite.h"
#include "libavutil/opt.h"
#include "libavutil/pixdesc.h"
#include "avcodec.h"
#include "bytestream.h"
#include "internal.h"
#include "amt.h"

typedef struct AMTDecContext {
	AVClass *av_class;

	int width, height;
	int chroma_type;
	int padding_type;
	int scan_dir;

	int (*decode)(struct AMTDecContext*, AVFrame *pic, AVPacket *pkt);
} AMTDecContext;

/*****************************************************************************/
static int amt_decode_bgr8(AMTDecContext *this, AVFrame *pic, AVPacket *pkt)
{
	const int pixelsize = 3;
	int i, n = pixelsize * this->width;
	int totalsize, wordsize;

	/* start of actual pixel data in AMT file */
	const uint8_t *data = pkt->data + 0x200;
	const uint8_t *src = data;

	uint8_t *ptr = pic->data[0];
	int stride = pic->linesize[0];
	if (this->scan_dir != 0)
	{
		/* image is bottom to top */
		ptr = pic->data[0] + pic->linesize[0] * (this->height - 1);
		stride = -stride;
	}

	switch (this->padding_type)
	{
	case AMT_PADDING_NONE:
		/* treat all pixel data as contiguous */
		wordsize = totalsize = this->width * this->height * pixelsize;
		break;

	case AMT_PADDING_32:
		wordsize = 4;
		totalsize = (this->width * this->height) * 4;
		break;

	case AMT_PADDING_128:
		wordsize = 16;
		totalsize = ((this->width * this->height * pixelsize + 15) / 16) * 16;
		break;

	case AMT_PADDING_256:
		wordsize = 32;
		totalsize = ((this->width * this->height * pixelsize + 31) / 32) * 32;
		break;

	default:
		return AVERROR_INVALIDDATA;
	}

	/* ensure size of input data is valid */
	if (pkt->size - 0x200 < totalsize)
	{
		return AVERROR_INVALIDDATA;
	}

	/* start copying pixel data from pkt to pic */
	for (i = 0; i < this->height; i++) {
		uint8_t *out = ptr;

		while (out < ptr + n) {
			/* how many more bytes are in the current word? */
			int bytes, maxbytes = wordsize - ((src - data) % wordsize);

			/* read as many remaining pixels as we can get from the current word */
			bytes = FFMIN((maxbytes / pixelsize) * pixelsize, ptr + n - out);
			memcpy(out, src, bytes);
			src += bytes;
			out += bytes;

			/* no more pixels in word -> skip to next one */
			if (maxbytes - bytes < pixelsize) {
				src += (maxbytes - bytes);
			}
		}

		ptr += stride;
	}

	return 0;
}

/*****************************************************************************/
static int amt_decode_yuv10(AMTDecContext *this, AVFrame *pic, AVPacket *pkt)
{
	int line = 0, pixel = 0, i;
	uint32_t val;

	/* start of actual pixel data in AMT file */
	const uint8_t *data = pkt->data + 0x200;
	const uint32_t *src = (const uint32_t *)data;

	uint16_t *y, *u, *v;
	int stride[3];

	if (this->scan_dir == 0)
	{
		/* image is top to bottom */
		y = (uint16_t*)pic->data[0];
		u = (uint16_t*)pic->data[1];
		v = (uint16_t*)pic->data[2];

		stride[0] = pic->linesize[0] >> 1;
		stride[1] = pic->linesize[1] >> 1;
		stride[2] = pic->linesize[2] >> 1;
	}
	else
	{
		/* image is bottom to top */
		y = (uint16_t*)(pic->data[0] + pic->linesize[0] * (this->height - 1));
		u = (uint16_t*)(pic->data[1] + pic->linesize[1] * (this->height - 1));
		v = (uint16_t*)(pic->data[2] + pic->linesize[2] * (this->height - 1));
		
		stride[0] = -(pic->linesize[0] >> 1);
		stride[1] = -(pic->linesize[1] >> 1);
		stride[2] = -(pic->linesize[2] >> 1);
	}

	/* ensure size of input data is valid (assumes RPlay native padding) */
	if (pkt->size - 0x200 < (this->width * this->height / 6) * 16)
	{
		return AVERROR_INVALIDDATA;
	}

	/* start copying pixel data from pkt to pic */
	while (line < this->height)
	{
		/* TODO: handle other chroma types */
		uint16_t x_y[6];
		uint16_t x_cb[3];
		uint16_t x_cr[3];

		/* unpack 6 pixels from a 128-bit word */
		val = AV_RL32(src);
		src++;
		x_cb[0] = (val >> 0) & 0x3ff;
		x_y[0] = (val >> 10) & 0x3ff;
		x_cr[0] = (val >> 20) & 0x3ff;
		x_y[1] = val >> 30;
		val = AV_RL32(src);
		src++;
		x_y[1] |= (val << 2) & 0x3fc;
		x_cb[1] = (val >> 8) & 0x3ff;
		x_y[2] = (val >> 18) & 0x3ff;
		x_cr[1] = val >> 28;
		val = AV_RL32(src);
		src++;
		x_cr[1] |= (val << 4) & 0x3f0;
		x_y[3] = (val >> 6) & 0x3ff;
		x_cb[2] = (val >> 16) & 0x3ff;
		x_y[4] = val >> 26;
		val = AV_RL32(src);
		src++;
		x_y[4] |= (val << 6) & 0x3c0;
		x_cr[2] = (val >> 4) & 0x3ff;
		x_y[5] = (val >> 14) & 0x3ff;

		for (i = 0; i < 6; i++)
		{
			y[pixel] = x_y[i];
			u[pixel >> 1] = x_cb[i >> 1];
			v[pixel >> 1] = x_cr[i >> 1];

			/* end of scanline: unpacking word into beginning of next line */
			if (++pixel == this->width) {
				y += stride[0];
				u += stride[1];
				v += stride[2];

				pixel = 0;
				if (++line == this->height)
				{
					/* end of frame */
					break;
				}
			}
		}
	}

	return 0;
}

/*****************************************************************************/
static int amt_decode_frame(AVCodecContext *avctx, void *data, int *got_frame,
	AVPacket *avpkt)
{
	AMTHeader_t *header = (AMTHeader_t*)avpkt->data;
	int version = AV_VERSION_INT(AV_RL16(&header->version_major), AV_RL16(&header->version_minor), 0);
	AMTDecContext *s = avctx->priv_data;
	AVFrame *p = data;
	int use_colorspace = 0;
	int ret; 

	if (avpkt->size < sizeof(header))
	{
		av_log(avctx, AV_LOG_ERROR, "not enough data to read header\n");
		return AVERROR_INVALIDDATA;
	}

	if (memcmp(header->id, AMT_FORMAT_ID, 4))
	{
		av_log(avctx, AV_LOG_ERROR, "not an AMT file\n");
		return AVERROR_INVALIDDATA;
	}

	if (avpkt->size - 0x200 < AV_RL32(&header->image_size))
	{
		av_log(avctx, AV_LOG_ERROR, "not enough data to read image\n");
		return AVERROR_INVALIDDATA;
	}

	/* Check version number in header */
	if (version > AV_VERSION_INT(AMT_FORMAT_VERSION_MAJOR, AMT_FORMAT_VERSION_MINOR, 0))
	{
		av_log(avctx, AV_LOG_ERROR, "unsupported AMT format version (%u.%u)\n",
			AV_RL16(&header->version_major), AV_RL16(&header->version_major));
		return AVERROR_INVALIDDATA;
	}

	s->width = AV_RL32(&header->width);
	s->height = AV_RL32(&header->height);
	s->chroma_type = AV_RL32(&header->chroma_type);
	s->padding_type = AV_RL32(&header->padding_type);
	
	if (s->padding_type >= AMT_PADDING_COUNT)
	{
		av_log(avctx, AV_LOG_ERROR, "invalid padding type\n");
		return AVERROR_INVALIDDATA;
	}

#define AMT_FORMAT(a, b) ((a << 8) | b)

	/* Pixel format */
	switch (AMT_FORMAT(AV_RL32(&header->pixel_format), AV_RL32(&header->chroma_type)))
	{
	case AMT_FORMAT(AMT_FMT_BGR8, 0):
		avctx->pix_fmt = AV_PIX_FMT_BGR24;
		s->decode = amt_decode_bgr8;
		break;

	case AMT_FORMAT(AMT_FMT_YUV10, AMT_CHROMA_422):
		avctx->pix_fmt = AV_PIX_FMT_YUV422P10;
		s->decode = amt_decode_yuv10;
		use_colorspace = 1;
		break;

	default:
		av_log(avctx, AV_LOG_ERROR, "unsupported pixel format or chroma type.\n");
		return AVERROR_INVALIDDATA;
	}

#undef AMT_FORMAT
	
	/* YUV colorspace settings */
	avctx->colorspace      = AVCOL_SPC_UNSPECIFIED;
	avctx->color_primaries = AVCOL_PRI_UNSPECIFIED;
	avctx->color_trc       = AVCOL_TRC_UNSPECIFIED;
	avctx->color_range     = AVCOL_RANGE_UNSPECIFIED;
	
	if (use_colorspace)
	{
		int colorspace  = AMT_COLOR_REC_709;
		int color_range = AMT_COLOR_RANGE_PARTIAL;
		if (version >= AV_VERSION_INT(1, 1, 0))
		{
			colorspace  = AV_RL32(&header->colorspace);
			color_range = AV_RL32(&header->color_range);
		}
		
		switch (colorspace)
		{
		/* TODO: DCI P3 */
		case AMT_COLOR_REC_709:
			avctx->colorspace      = AVCOL_SPC_BT709;
			avctx->color_primaries = AVCOL_PRI_BT709;
			avctx->color_trc       = AVCOL_TRC_BT709;
			break;
			
		case AMT_COLOR_REC_601:
			avctx->colorspace      = AVCOL_SPC_SMPTE170M;
			avctx->color_primaries = AVCOL_PRI_SMPTE170M;
			avctx->color_trc       = AVCOL_TRC_SMPTE170M;
			break;
		
		case AMT_COLOR_REC_2020:
			avctx->colorspace      = AVCOL_SPC_BT2020_NCL;
			avctx->color_primaries = AVCOL_PRI_BT2020;
			avctx->color_trc       = AVCOL_TRC_BT2020_10;
			break;
		
		default:
			av_log(avctx, AV_LOG_ERROR, "unsupported color space.\n");
			return AVERROR_INVALIDDATA;
		}
		
		if (color_range == AMT_COLOR_RANGE_FULL)
		{
			avctx->color_range = AVCOL_RANGE_JPEG;
		}
		else
		{
			avctx->color_range = AVCOL_RANGE_MPEG;
		}
	}
	
	/* TODO: scan type */
	
	if ((ret = ff_set_dimensions(avctx, s->width, s->height)) < 0)
		return ret;

	if ((ret = ff_get_buffer(avctx, p, 0)) < 0)
		return ret;
	p->pict_type = AV_PICTURE_TYPE_I;

	if ((ret = s->decode(s, p, avpkt)) < 0)
		return ret;

	*got_frame = 1;
	return avpkt->size;
}

/*****************************************************************************/
static const AVOption amtdec_options[] = {
	/* TODO */
	{ NULL }
};

static const AVClass amtdec_class = {
	"amt decoder",
	av_default_item_name,
	amtdec_options,
	LIBAVUTIL_VERSION_INT,
};

AVCodec ff_amt_decoder = {
	.name = "amt",
	.long_name = NULL_IF_CONFIG_SMALL("Alcorn McBride AMT image"),
	.type = AVMEDIA_TYPE_VIDEO,
	.id = AV_CODEC_ID_AMT,
	.priv_data_size = sizeof(AMTDecContext),
	.decode = amt_decode_frame,
	.capabilities = AV_CODEC_CAP_DR1,
	.priv_class = &amtdec_class,
};
