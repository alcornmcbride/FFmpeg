
#ifndef AVCODEC_AMT_H
#define AVCODEC_AMT_H

#define AMT_FORMAT_ID "AMT_"
#define AMT_FORMAT_VERSION_MAJOR 1
#define AMT_FORMAT_VERSION_MINOR 1

enum AMTFormat {
	AMT_FMT_BGR8,
	AMT_FMT_BGR10,
	AMT_FMT_BGR12,
	AMT_FMT_BGRA8,
	AMT_FMT_BGRA10,
	AMT_FMT_BGRA12,
	AMT_FMT_YUV8,
	AMT_FMT_YUV10,
	AMT_FMT_YUV12,

	AMT_FMT_COUNT
};

enum AMTChroma {
	AMT_CHROMA_444,
	AMT_CHROMA_422,
	AMT_CHROMA_420,

	AMT_CHROMA_COUNT
};

enum AMTPadding {
	AMT_PADDING_NONE,
	AMT_PADDING_32,
	AMT_PADDING_128,
	AMT_PADDING_256,

	AMT_PADDING_COUNT
};
#define AMT_PADDING_NATIVE AMT_PADDING_128

enum AMTColorspace {
	AMT_COLOR_REC_709,
	AMT_COLOR_REC_601,
	AMT_COLOR_REC_2020,
	AMT_COLOR_DCI_P3,

	AMT_COLOR_COUNT
};

enum AMTColorRange {
	AMT_COLOR_RANGE_FULL,
	AMT_COLOR_RANGE_PARTIAL,
	
	AMT_COLOR_RANGE_COUNT
};

enum AMTScanType {
	AMT_PROGRESSIVE,
	AMT_INTERLACE_EVEN,
	AMT_INTERLACE_ODD,

	AMT_SCAN_COUNT
};

#pragma pack(push,1)
typedef struct AMTHeader_s {
	char id[4];
	uint16_t version_major, version_minor;
	uint32_t file_size;
	uint32_t width, height;
	uint32_t pixel_format;
	uint32_t chroma_type;
	uint32_t scan_dir;
	uint32_t padding_type;
	uint32_t compress;
	uint32_t image_size;
	uint32_t color_range;
	uint32_t colorspace;
	uint32_t scan_type;
} AMTHeader_t;
#pragma pack(pop)

#endif /* AVCODEC_AMT_H */
